// @flow
import React from 'react';

import { Route, Switch } from 'fusion-plugin-react-router';
import { Helmet } from 'fusion-plugin-react-helmet-async';


import Home from './pages/home.js';
import PageNotFound from './pages/pageNotFound.js';

const root = (
  <main>
    <Helmet>
      <title>Jay McMullen - Full Stack Web Developer</title>
      <meta name="description" content="Hi, my name is Jay and I like to party. I make fast, secure and scalable web applications."/>
      <link rel="canonical" href="https://jays.works/" />
      <meta name="viewport" content="width=device-width" />
    </Helmet>
    <Switch>
      <Route exact path="/" component={Home} />
      <Route component={PageNotFound} />
    </Switch>
  </main>
);
export default root;

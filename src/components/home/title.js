import React from 'react';
import { styled } from 'fusion-plugin-styletron-react';

import Socials from './socials';

const TitleStyle = styled('div', {
  position: 'fixed',
  top: '24%',
  padding: '2rem',
});

const Heading = styled('h1', {
  fontFamily: 'HelveticaNeue-Light, Arial',
  fontSize: '4rem',
  '@media (min-width: 768px)': {
    fontSize: '6rem',
  },
});

const Title = () => (
  <TitleStyle>
    <Heading>Jay McMullen</Heading>
    <Socials />
  </TitleStyle>
);

export default Title;

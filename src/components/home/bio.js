import React from 'react';
import { styled } from 'fusion-plugin-styletron-react';

const BioStyle = styled(
  'div', {
    padding: '1rem',
    backgroundColor: '#000',
    color: '#fff',
    fontFamily: 'HelveticaNeue-Light, Arial',
    fontSize: '1.2rem',
    zIndex: 2,
    marginTop: '100vh',
  }
)

const FeaturedText = styled(
  'h2', {
    fontSize: '1.4rem',
  }
)

const Bio = () => (
  <BioStyle>
    <FeaturedText>I make fast, secure and scalable web applications using the latest tech available.</FeaturedText>

    <p>Over the last 4 years, I've gained a ton of experience working with e-commerce, music & tech related start ups across Sydney, Australia and London in the United Kingdom.</p>

    <p>Technology I specialise in includes: Vue.js, Nuxt.js, React.js, Next.js, Fusion.js, Node.js, Lambda, Serverless, Express, Koa, GraphQL, MongoDB, MySQL, Webpack, Progressive Web Applications & RESTful API's.</p>
  </BioStyle>
);

export default Bio;
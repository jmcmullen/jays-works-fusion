import React from 'react';
import { styled } from 'fusion-plugin-styletron-react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {
  faGithub,
  faLinkedinIn,
  faSoundcloud,
  faTelegram,
} from '@fortawesome/free-brands-svg-icons';

const SocialsStyle = styled('div', {
  fontFamily: 'HelveticaNeue-Light, Arial',
  fontSize: '2rem',
  svg: {
    padding: '5px',
  },
});

const linkStyle = {
  padding: '0 10px',
  color: '#000',
};

const Socials = () => (
  <SocialsStyle>
    <a href="https://github.com/jmcmullen" style={linkStyle}>
      <FontAwesomeIcon icon={faGithub} />
    </a>
    <a href="https://linkedin.com/in/j-mcmullen" style={linkStyle}>
      <FontAwesomeIcon icon={faLinkedinIn} />
    </a>
  </SocialsStyle>
);

export default Socials;

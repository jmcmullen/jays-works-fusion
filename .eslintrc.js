module.exports = {
  extends: [require.resolve('eslint-config-fusion')],
  plugins: ['prettier'],
  rules: {
    "prettier/prettier": "error"
  }
};